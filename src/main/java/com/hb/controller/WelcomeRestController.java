package com.hb.controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class WelcomeRestController {

	/*
	 * @GetMapping("/welcome") 
	 * public ModelAndView welcomeMsg(Model model) {
	 * ModelAndView mav = new ModelAndView();
	 * mav.addObject("welcomeMsg",
	 * "Welcome to Ashok IT");
	 *  mav.setViewName("index");
	 * 
	 * return mav; }
	 */
	

	@GetMapping("/welcome")
	public String welcomeMsg() {
		String msg = "Welcome to Ashok IT";
		System.out.println("Welcome to ashok it");
		return msg;
	}
	
}
